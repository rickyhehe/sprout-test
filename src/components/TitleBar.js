import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default listItem = (props) => (
    <View style={styles.titleBar}>
        <Text style={styles.titleText}>CONTACTS</Text>
    </View>

)

const styles = StyleSheet.create({
    titleBar: {
        width: '100%',
        backgroundColor: '#68d9bb',
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    titleText: {
        color: "#fff",
        fontSize: 15
    }
})
// export default listItem
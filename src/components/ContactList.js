import React, { Component } from 'react'
import { View, Image, Button, ScrollView, StyleSheet, Text, TouchableOpacity } from 'react-native'
import axios from 'axios'
import phoneIc from '../assets/phone_ic.png'
import chatIc from '../assets/chat_ic.png'
class ContactList extends Component {
    state = {
        selectedList: 2,
        contactList: []
    }


    componentDidMount() {
        axios.get('https://reqres.in/api/users?per_page=12')
            .then((response) => {
                // handle success
                this.setState({
                    contactList: response.data.data
                })
            })

    }
    selectContact = (id) => {
        this.setState({
            selectedList: id
        })
    }
    isSelected = (id, styleName) => {
        return this.state.selectedList == id ? styles[styleName] : 0
    }
    getContactList = () => {
        return this.state.contactList.map((data) => {
            return (
                <TouchableOpacity key={data.id} onPress={() => this.selectContact(data.id)} style={[styles.listItem, this.isSelected(data.id, 'selectedList')]}>
                    <Image style={styles.contactImage} source={{ uri: data.avatar }} />

                    <View style={styles.textContainer}>
                        <Text style={[styles.textBold, this.isSelected(data.id, 'textWhite')]}>{data.first_name} {data.last_name}</Text>
                        <Text style={[styles.textSmall, this.isSelected(data.id, 'textWhite')]}>{data.email}</Text>
                    </View>

                    <View style={styles.iconContainer}>
                        <Image style={styles.iconImage} source={phoneIc} />
                        <Image style={styles.iconImage} source={chatIc} />

                    </View>
                </TouchableOpacity>

            )
        })
    }


    render() {
        return (
            <ScrollView style={styles.container}>
                {this.getContactList()}
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        width: "100%",
    },
    contactImage: {
        width: 100,
        height: 100,
        marginRight: 10
    },

    iconImage: {
        width: 40,
        height: 40,
        marginLeft: 10
    },
    listItem: {
        flex: 1,
        backgroundColor: "#eee",
        flexDirection: "row",
        alignItems: 'center'
    },
    selectedList: {
        backgroundColor: "#832ac7",

    },
    iconContainer: {
        flexDirection: "row",
        marginLeft: "auto"
    },
    textWhite: {
        color: 'white'
    },
    textSmall: {
        fontSize: 12,
    },
    textBold: {
        fontWeight: "bold"
    }
})
export default ContactList
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text
} from 'react-native';

import ContactList from './src/components/ContactList'
import TitleBar from './src/components/TitleBar'
const App = () => {
    return (
        <View>
            <TitleBar />
            <ScrollView>
                <ContactList />
            </ScrollView>

        </View>
    );
};

const styles = StyleSheet.create({

});

export default App;
